package jp.alhinc.okita_yohei.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {
	public static void main(String[] args)  {
		//＜＜コマンドライン引数が渡されていない場合「予期せぬエラーが発生しました」＞＞
		if(args.length != 1) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}

		Map<String, String> branchNames = new HashMap<>();
		Map<String, Long> branchSales = new HashMap<>();
		Map<String, String> commodityNames = new HashMap<>();
		Map<String, Long> commoditySales = new HashMap<>();

		//input実行
		if (!input(args[0], "branch.lst", "支店", "^\\d{3}$", branchNames, branchSales)) {
			return;
		}
		if (!input(args[0], "commodity.lst", "商品", "^[a-zA-Z0-9]{8}+$", commodityNames, commoditySales)) {
			return;
		}

		//売り上げファイルから支店情報、商品情報を抽出
		File[] files = new File(args[0]).listFiles();
		List<File> rcdFiles = new ArrayList<>();
		for(int i = 0; i < files.length; i++) {
			if(files[i].isFile() && files[i].getName().matches("^\\d{8}.rcd$")) {
				rcdFiles.add(files[i]);
			}
		}
		Collections.sort(rcdFiles);
		//＜＜売上ファイルが連番になっていない場合「売上ファイルが連番になっていません」＞＞
		for(int i = 0; i < rcdFiles.size() - 1; i++) {
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0,8));
			int latter = Integer.parseInt((rcdFiles.get(i + 1).getName().substring(0,8)));
			if((latter - former) != 1) {
				System.out.println("売上ファイルが連番になっていません");
				return;
			}
		}

		//rcdFiles(売上ファイル)の内容を読み込み
		BufferedReader br = null;
		for(int i = 0; i < rcdFiles.size(); i++) {
			File file = rcdFiles.get(i);
			try {
				FileReader frs = new FileReader(file);
				br = new BufferedReader(frs);
				String line;
				List<String> lines = new ArrayList<>();
				while((line = br.readLine()) != null) {
					lines.add(line);
				}
				//＜＜売上ファイルが3行じゃなければ「＜ファイル名＞のフォーマットが不正です」＞＞
				if(lines.size() != 3) {
					System.out.println(rcdFiles.get(i).getName() + "のフォーマットが不正です");
					return;
				}
				//＜＜Map(branchNames)に支店コードのKeyがなければ「＜ファイル名＞の支店コードが不正です」＞＞
				if(!branchNames.containsKey(lines.get(0))) {
					System.out.println(file.getName() + "の支店コードが不正です");
					return;
				}
				//＜＜Map(commodityNames)に商品コードのKeyがなければ「＜ファイル名＞の商品コードが不正です」＞＞
				if(!commodityNames.containsKey(lines.get(1))) {
					System.out.println(file.getName() + "の商品コードが不正です");
					return;
				}
				//＜＜売上金額が数字でなければ「予期せぬエラーが発生しました」＞＞
				if(!lines.get(2).matches("^[0-9]*$")) {
					System.out.println("予期せぬエラーが発生しました。");
					return;
				}
				long fileSale = Long.parseLong(lines.get(2));
				//System.out.println(lines.get(2));
				Long saleBranchAmount = branchSales.get(lines.get(0)) + fileSale;
				//＜＜支店売上金額の合計が１０桁を超える場合「合計金額が10桁を超えました」＞＞
				if(saleBranchAmount >= 10000000000L) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}
				branchSales.put(lines.get(0), saleBranchAmount);
				Long saleCommodityAmount = commoditySales.get(lines.get(1)) + fileSale;
				//＜＜商品売上金額の合計が１０桁を超える場合「合計金額が10桁を超えました」＞＞
				if(saleCommodityAmount >= 10000000000L) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}
				commoditySales.put(lines.get(1), saleCommodityAmount);
			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			} finally {
				if(br != null) {
					try {
						br.close();
					} catch (IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return;
					}
				}
			}
		}

		//output実行
		if (!output(args[0], "branch.out", branchNames, branchSales)) {
			return;
		}

		if (!output(args[0], "commodity.out", commodityNames, commoditySales)) {
			return;
		}
	}
	//支店定義ファイル読み込み
	private static boolean input(String path, String fileName, String status, String format, Map<String, String> names, Map<String, Long> sales) {
		BufferedReader br = null;
		try {
			File file = new File(path, fileName);
			//＜＜定義ファイルが存在しない場合は「＜支店or商品＞定義ファイルが存在しません」＞＞
			if(!file.exists()) {
				System.out.println(status + "定義ファイルが存在しません");
				return false;
			}
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);
			String line;
			while((line = br.readLine()) != null) {
				String[] items = line.split(",");

				//＜＜定義ファイルのフォーマットが規定以外なら「＜支店or商品＞定義ファイルのフォーマットが不正です」＞＞
				if((items.length != 2 || !items[0].matches(format))) {
					System.out.println(status + "定義ファイルのフォーマットが不正です");
					return false;
				}
				names.put(items[0], items[1]);
				sales.put(items[0], 0L);
			}
		br.close();
		} catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if(br != null) {
				try {
					br.close();
				} catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}

	//rcdFiles(売上ファイル)の内容を集計ファイルを出力
	private static boolean output(String path, String fileName, Map<String, String> names, Map<String, Long> sales) {
		BufferedWriter bw = null;
		try {
			File file = new File(path, fileName);
			bw = new BufferedWriter(new FileWriter(file));
			for(String key : names.keySet()) {
				if (names.containsKey(key)) {
					bw.write(key + "," + names.get(key) + "," + sales.get(key));
				}
				bw.newLine();
			}
		} catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if(bw != null) {
				try {
					bw.close();
				}catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}
}